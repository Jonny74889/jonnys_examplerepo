# Jonnys_ExampleRepo

Repo contains example files/scripts which showcase how to use some packages or functions.

- bcrypt_example.py -> Example on how to use bcrypt for password encryption and verification
- check_os.py -> Check in which os the program is executed
- load_env_file_example.py -> load and .env file via python
- load_env_vars.py -> load set OS variables
