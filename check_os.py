#import os library
import os

#in some cases you might want to identify in which OS the program is executed
#i usually use this to decide wheter .env files should be loaded or not
#in linux i will use source to load the files in windows there is no good option.. or at least i am not aware of it
#check if windows in OS 
if 'Windows' in os.environ.get('OS', ''):
    print('This is a windows system')
else:
    print('This is something else...')
