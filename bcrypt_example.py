#install Bcrypt via pip install Bcrypt
#import Bcrypt
from flask_bcrypt import Bcrypt

#get instance of class
inst = Bcrypt()

#in the code of bcrypt it seems that the max pw length is 72..
#this means only the 72 first characters are considered for hashing and comparison
password_plain = 'test_pw'
password_encrypted = ''
print('This test password will be encrypted: '+password_plain)

password_encrypted = inst.generate_password_hash(password_plain)
print('After the encryption the initial password looks now like this: ')
print(password_encrypted)

print('Call function to verify if the stored/encrypted password fits to the plain text password')
print(inst.check_password_hash(password_encrypted, password_plain))

print('------------------------------------------------')
print('You can encrypt the encrypted password several times to increase security')
print('This time we call the generate_password_hash method and pass the encryption rounds.')
password_encrypted_10 = inst.generate_password_hash(password_plain, 10)
print(password_encrypted_10)

print('Encrpytion works the same as before. No need to pass the rounds.')
print(inst.check_password_hash(password_encrypted_10, password_plain))
