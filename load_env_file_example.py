#import os library
import os
#import load_dotenv function
from dotenv import load_dotenv
#import Path class
from pathlib import Path
#set path to .env file
env_path = Path('.\\.env_test')
#load the file
load_dotenv(dotenv_path=env_path)
#access the now loaded environment variables
password = os.getenv('PASSWORD')
testvar = os.getenv('TESTVAR')

#print values
print('PASSWORD: '+password)
print('TESTVAR: '+testvar)
